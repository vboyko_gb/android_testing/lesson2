package com.geekbrains.tests

import com.nhaarman.mockito_kotlin.mock
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Before
    fun bef() {
        MockitoAnnotations.openMocks(this)
    }


    @Test
    fun addition_isCorrect() {
        val MOCKITO = "mockito"
        val ESPRESSO = "espresso"
        val MOCKITO_INT = 1
        val ESPRESSO_INT = 2
        val coffee = Mockito.mock(Coffee::class.java)
        Mockito.`when`(coffee.getCoffeeName()).thenReturn("Капучино")
        assertEquals(coffee.getCoffeeName(), "Капучин123о")
    }


    @Test
    fun testReturnValueDependentOnMethodParameter() {
        val MOCKITO = "mockito"
        val ESPRESSO = "espresso"
        val MOCKITO_INT = 1
        val ESPRESSO_INT = 2
        val coffee = Mockito.mock(Coffee::class.java)
        val comparable = Mockito.mock(Comparable::class.java) as Comparable<String>
        Mockito.`when`(comparable.compareTo(MOCKITO)).thenReturn(MOCKITO_INT)
        Mockito.`when`(comparable.compareTo(ESPRESSO)).thenReturn(ESPRESSO_INT)

        assertEquals(MOCKITO_INT, comparable.compareTo(MOCKITO))
        assertEquals(ESPRESSO_INT, comparable.compareTo(ESPRESSO))
    }


}


class Coffee {

    fun getCoffeeName() = "12312"

}